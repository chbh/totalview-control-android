package dk.bws.totalviewcontrol;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.widget.ImageButton;
import android.graphics.Color;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;




public class MessageActivity extends WearableActivity
        implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    GoogleApiClient googleClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Build a new GoogleApiClient that includes the Wearable API
        googleClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    // Connect to the data layer when the Activity starts
    @Override
    protected void onStart() {
        super.onStart();
        googleClient.connect();
    }

    // Send a message when the data layer connection is successful.
    @Override
    public void onConnected(Bundle connectionHint) {
        String message = "Hello wearable\n Via the data layer";
        //Requires a new thread to avoid blocking the UI
        new SendToDataLayerThread("/message_path", message).start();
    }

    // Disconnect from the data layer when the Activity stops
    @Override
    protected void onStop() {
        if (null != googleClient && googleClient.isConnected()) {
            googleClient.disconnect();
        }
        super.onStop();
    }


    public void availableOnClick(View v)
    {
        setColor("available");
        String message = "available";
        SendToDataLayerThread send = new SendToDataLayerThread("/message_path", message);
        send.start();
    }

    public void lunchOnClick(View v)
    {
        setColor("lunch");
        String message = "lunch";
        SendToDataLayerThread send = new SendToDataLayerThread("/message_path", message);
        send.start();
    }

    public void appointmentOnClick(View v)
    {
        setColor("appointment");
        String message = "appointment";
        SendToDataLayerThread send = new SendToDataLayerThread("/message_path", message);
        send.start();
    }

    public void offDutyOnClick(View v)
    {
        setColor("off duty");
        String message = "off duty";
        SendToDataLayerThread send = new SendToDataLayerThread("/message_path", message);
        send.start();
    }

    private void setColor(String btnId)
    {
        ImageButton btnAvailable = (ImageButton) findViewById(R.id.btnAvailable);
        ImageButton btnLunch = (ImageButton) findViewById(R.id.btnLunch);
        ImageButton btnAppointment = (ImageButton) findViewById(R.id.btnAppointment);
        ImageButton btnOffDuty = (ImageButton) findViewById(R.id.btnOffDuty);

        switch(btnId.toLowerCase())
        {
            case "available":
                btnAvailable.setColorFilter(Color.parseColor("#70B894"));
                btnLunch.setColorFilter(Color.TRANSPARENT);
                btnAppointment.setColorFilter(Color.TRANSPARENT);
                btnOffDuty.setColorFilter(Color.TRANSPARENT);
                break;
            case "lunch":
                btnAvailable.setColorFilter(Color.TRANSPARENT);
                btnLunch.setColorFilter(Color.parseColor("#60FF60"));
                btnAppointment.setColorFilter(Color.TRANSPARENT);
                btnOffDuty.setColorFilter(Color.TRANSPARENT);
                break;
            case "appointment":
                btnAvailable.setColorFilter(Color.TRANSPARENT);
                btnLunch.setColorFilter(Color.TRANSPARENT);
                btnAppointment.setColorFilter(Color.parseColor("#7070FF"));
                btnOffDuty.setColorFilter(Color.TRANSPARENT);
                break;
            case "off duty":
                btnAvailable.setColorFilter(Color.TRANSPARENT);
                btnLunch.setColorFilter(Color.TRANSPARENT);
                btnAppointment.setColorFilter(Color.TRANSPARENT);
                btnOffDuty.setColorFilter(Color.parseColor("#70FFFF"));
                break;
        }
    }

    // Placeholders for required connection callbacks
    @Override
    public void onConnectionSuspended(int cause) { }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) { }

    class SendToDataLayerThread extends Thread {
        String path;
        String message;

        // Constructor to send a message to the data layer
        SendToDataLayerThread(String p, String msg) {
            path = p;
            message = msg;
        }

        public void run() {
            NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(googleClient).await();
            for (Node node : nodes.getNodes()) {
                MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(googleClient, node.getId(), path, message.getBytes()).await();
                if (result.getStatus().isSuccess()) {
                    Log.e("myTag", "Message: {" + message + "} sent to: " + node.getDisplayName());
                } else {
                    // Log an error
                    Log.e("myTag", "ERROR: failed to send Message");
                }
            }
        }
    }
}