package dk.bws.totalviewcontrol;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.util.Objects;

/**
 * Created by mroj on 19-05-2016.
 */
public class MainActivity extends AppCompatActivity {

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Register the local broadcast receiver
        IntentFilter messageFilter = new IntentFilter(Intent.ACTION_SEND);
        MessageReceiver messageReceiver = new MessageReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, messageFilter);
        mTextView = (TextView)findViewById(R.id.lblHello);
    }

    public class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            Uri state = Uri.parse("https://testweb.bws.dk/TotalViewControl/Home/Available");
            if(Objects.equals(message, "available")){

            }else if(Objects.equals(message, "lunch")){
                state = Uri.parse("https://testweb.bws.dk/TotalViewControl/Home/Lunch");
            }else if(Objects.equals(message, "appointment")){
                state = Uri.parse("https://testweb.bws.dk/TotalViewControl/Home/Appointment");
            }else if(Objects.equals(message, "off duty")){
                state = Uri.parse("https://testweb.bws.dk/TotalViewControl/Home/Offduty");
            }
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, state);
            startActivity(browserIntent);
            Log.v("myTag", "Main activity received message: " + message);
            // Display message in UI

            mTextView.setText(message);
        }
    }
}
